#!/bin/bash

python -c "import configparser; c = configparser.ConfigParser(); c.read('setup.cfg'); [print(f'\"{d}\"') for d in c['options']['install_requires'].split('\n') if len(d) > 0]" 
