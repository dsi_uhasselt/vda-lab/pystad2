"""
A module containing to configure how STAD uses a lens / filter.

Classes
------
Lens :
  A class that configures a 1-dimensional lens / filter.
MultiLens:
  A class to combine multiple lenses
"""
from .Lens import Lens
from .MultiLens import MultiLens